#!/bin/sh
# Copyright (C) 2018 PUC-Rio/LAC
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

getmaxmem()
{
  perl -wlne '/\b(\d+)maxresident\b/ and print $1;' $1
}

getruntime()
{
  perl -wlne '/(\d+):(\d+.\d+)elapsed/ and print (($1*60)+$2);' $1
}

PATS=2                          # last pattern
FAILS=3                         # last failure
RUNS=5                          # last run

for pat in $(seq 0 $PATS); do
  for suf in '' '.x'; do
    out="pat${pat}$suf.data"
    rm -f "$out"
    for fail in $(seq 0 $FAILS); do
      for run in $(seq 1 $RUNS); do

        # Input files.
        fup="up.${run}.pat${pat}.fail${fail}$suf"
        test -f "$fup" || (echo 2>1 "error: no such file $fup"; exit 1)

        fdw="down.${run}.pat${pat}.fail${fail}$suf"
        test -f "$fdw" || (echo 2>1 "error: no such file $fdw"; exit 1)

        # Update counters.
        if test "$run" -eq 1; then
          tim_up_sum=`getruntime "$fup"`
          tim_up_lo=$tim_up_sum
          tim_up_hi=$tim_up_sum

          tim_dw_sum=`getruntime "$fdw"`
          tim_dw_lo=$tim_dw_sum
          tim_dw_hi=$tim_dw_sum

          mem_up_sum=`getmaxmem "$fup"`
          mem_up_lo=$mem_up_sum
          mem_up_hi=$mem_up_sum

          mem_dw_sum=`getmaxmem "$fdw"`
          mem_dw_lo=$mem_dw_sum
          mem_dw_hi=$mem_dw_sum
        else
          tim=`getruntime "$fup"`
          tim_up_sum=$(echo "$tim_up_sum + $tim" | bc)
          test $(echo "$tim < $tim_up_lo" | bc) = 1 && tim_up_lo=$tim
          test $(echo "$tim > $tim_up_hi" | bc) = 1 && tim_up_hi=$tim

          tim=`getruntime "$fdw"`
          tim_dw_sum=$(echo "$tim_dw_sum + $tim" | bc)
          test $(echo "$tim < $tim_dw_lo" | bc) = 1 && tim_dw_lo=$tim
          test $(echo "$tim > $tim_dw_hi" | bc) = 1 && tim_dw_hi=$tim

          mem=`getmaxmem "$fup"`
          mem_up_sum=$(echo "$mem_up_sum + $mem" | bc)
          test $(echo "$mem < $mem_up_lo" | bc) = 1 && mem_up_lo=$mem
          test $(echo "$mem > $mem_up_hi" | bc) = 1 && mem_up_hi=$mem

          mem=`getmaxmem "$fdw"`
          mem_dw_sum=$(echo "$mem_dw_sum + $mem" | bc)
          test $(echo "$mem < $mem_dw_lo" | bc) = 1 && mem_dw_lo=$mem
          test $(echo "$mem > $mem_dw_hi" | bc) = 1 && mem_dw_hi=$mem
        fi
      done

      tim_up=$(echo "scale=2;$tim_up_sum/$RUNS" | bc)
      tim_dw=$(echo "scale=2;$tim_dw_sum/$RUNS" | bc)

      mem_up=$(echo "scale=1;($mem_up_sum/$RUNS)/1000" | bc)
      mem_up_lo=$(echo "scale=1;$mem_up_lo/1000" | bc)
      mem_up_hi=$(echo "scale=1;$mem_up_hi/1000" | bc)

      mem_dw=$(echo "scale=1;($mem_dw_sum/$RUNS)/1000" | bc)
      mem_dw_lo=$(echo "scale=1;$mem_dw_lo/1000" | bc)
      mem_dw_hi=$(echo "scale=1;$mem_dw_hi/1000" | bc)

      # Write to output file.
      echo $fail\
           $tim_up\
           $tim_up_lo\
           $tim_up_hi\
           $tim_dw\
           $tim_dw_lo\
           $tim_dw_hi\
           $mem_up\
           $mem_up_lo\
           $mem_up_hi\
           $mem_dw\
           $mem_dw_lo\
           $mem_dw_hi\
           >>$out
    done
  done
done
