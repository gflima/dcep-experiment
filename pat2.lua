_G.version = '2018-01-18, 04:23 UTC'
--[[ Copyright (C) 2018 PUC-Rio/LAC

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.  ]]--

local automata = require'automata'
_ENV = nil

-- Constructs an automata that matches "a+b+c+".
return function ()
   return automata:new {
      final = {4},
      graph = {
         a={2, 2, 2, 2},
         b={1, 3, 3, 3},
         c={1, 2, 4, 4}
      },
   }
end

-- Local Variables:
-- eval: (add-hook 'write-file-functions 'time-stamp)
-- time-stamp-start: "_G.version = '"
-- time-stamp-format: "%:y-%02m-%02d, %02H:%02M"
-- time-stamp-time-zone: "UTC'"
-- time-stamp-end: " UTC"
-- End:
