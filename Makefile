num=     10000000
size=    100
slide=   20
ack=    -1000
xack=    40

fail1= 2500000
fail2= 2500000,5000000
fail3= 2500000,5000000,7500000

DEBUG=
FAIL0=
FAIL1= "-f $(fail1)"
FAIL2= "-f $(fail2)"
FAIL3= "-f $(fail3)"
PAT0=  "-f pat0"
PAT1=  "-f pat1"
PAT2=  "-f pat2"
XACK=  "-A $(xack)"

all:
	$(MAKE) cycle S=1
	$(MAKE) cycle S=2
	$(MAKE) cycle S=3
	$(MAKE) cycle S=4
	$(MAKE) cycle S=5
	$(MAKE) cycle S=6
	$(MAKE) cycle S=7
	$(MAKE) cycle S=8
	$(MAKE) cycle S=9
	$(MAKE) cycle S=10

cycle:
	$(MAKE) run P=9001 NAME=pat0.fail0   FAIL=$(FAIL0) PAT=$(PAT0) XACK=
	$(MAKE) run P=9002 NAME=pat0.fail0.x FAIL=$(FAIL0) PAT=$(PAT0) XACK=$(XACK)
	$(MAKE) run P=9003 NAME=pat0.fail1   FAIL=$(FAIL1) PAT=$(PAT0) XACK=
	$(MAKE) run P=9004 NAME=pat0.fail1.x FAIL=$(FAIL1) PAT=$(PAT0) XACK=$(XACK)
	$(MAKE) run P=9005 NAME=pat0.fail2   FAIL=$(FAIL2) PAT=$(PAT0) XACK=
	$(MAKE) run P=9006 NAME=pat0.fail2.x FAIL=$(FAIL2) PAT=$(PAT0) XACK=$(XACK)
	$(MAKE) run P=9007 NAME=pat0.fail3   FAIL=$(FAIL3) PAT=$(PAT0) XACK=
	$(MAKE) run P=9008 NAME=pat0.fail3.x FAIL=$(FAIL3) PAT=$(PAT0) XACK=$(XACK)
	$(MAKE) run P=9009 NAME=pat1.fail0   FAIL=$(FAIL0) PAT=$(PAT1) XACK=
	$(MAKE) run P=9010 NAME=pat1.fail0.x FAIL=$(FAIL0) PAT=$(PAT1) XACK=$(XACK)
	$(MAKE) run P=9011 NAME=pat1.fail1   FAIL=$(FAIL1) PAT=$(PAT1) XACK=
	$(MAKE) run P=9012 NAME=pat1.fail1.x FAIL=$(FAIL1) PAT=$(PAT1) XACK=$(XACK)
	$(MAKE) run P=9013 NAME=pat1.fail2   FAIL=$(FAIL2) PAT=$(PAT1) XACK=
	$(MAKE) run P=9014 NAME=pat1.fail2.x FAIL=$(FAIL2) PAT=$(PAT1) XACK=$(XACK)
	$(MAKE) run P=9015 NAME=pat1.fail3   FAIL=$(FAIL3) PAT=$(PAT1) XACK=
	$(MAKE) run P=9016 NAME=pat1.fail3.x FAIL=$(FAIL3) PAT=$(PAT1) XACK=$(XACK)
	$(MAKE) run P=9017 NAME=pat2.fail0   FAIL=$(FAIL0) PAT=$(PAT2) XACK=
	$(MAKE) run P=9018 NAME=pat2.fail0.x FAIL=$(FAIL0) PAT=$(PAT2) XACK=$(XACK)
	$(MAKE) run P=9019 NAME=pat2.fail1   FAIL=$(FAIL1) PAT=$(PAT2) XACK=
	$(MAKE) run P=9020 NAME=pat2.fail1.x FAIL=$(FAIL1) PAT=$(PAT2) XACK=$(XACK)
	$(MAKE) run P=9021 NAME=pat2.fail2   FAIL=$(FAIL2) PAT=$(PAT2) XACK=
	$(MAKE) run P=9022 NAME=pat2.fail2.x FAIL=$(FAIL2) PAT=$(PAT2) XACK=$(XACK)
	$(MAKE) run P=9023 NAME=pat2.fail3   FAIL=$(FAIL3) PAT=$(PAT2) XACK=
	$(MAKE) run P=9024 NAME=pat2.fail3.x FAIL=$(FAIL3) PAT=$(PAT2) XACK=$(XACK)

clean:
	rm -f down.* up.*

time= /usr/bin/time
up= $(time) -a -o "up.$(S).$(NAME)"\
   ./upstream -S $(S) -p $(P) -D $(DEBUG) -n $(num)
down= $(time) -a -o "down.$(S).$(NAME)"\
  ./downstream -S $(S) -u localhost:$(P) -D $(DEBUG) -s $(slide) -w $(size) -a $(ack)

run:
	$(up) $(FAIL) >"up.$(S).$(NAME)" &
	$(down) $(XACK) $(PAT) >"down.$(S).$(NAME)"
	@sleep 1

.PHONY: all cycle clean run
