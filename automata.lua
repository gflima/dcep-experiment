_G.version = '2018-01-18, 00:42 UTC'
--[[ Copyright (C) 2018 PUC-Rio/LAC

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.  ]]--

local assert = assert
local pairs = pairs
local setmetatable = setmetatable
local table = table
local type = type

local automata = {}
_ENV = nil
do
   automata.__index = automata
   automata.__metatable = 'not your business'
end

function automata:new (t)
   assert (type (t) == 'table')
   assert (type (t.final) == 'table')
   assert (type (t.graph) == 'table')
   local obj = {
      final = t.final,          -- final state
      graph = t.graph,          -- graph
      curr = 1,                 -- current state
      match = {},               -- current match
   }
   return setmetatable (obj, automata)
end

function automata:accepted ()
   for i=1,#self.final do
      if self.curr == self.final[i] then
         return self.match
      end
   end
   return nil
end

function automata:dump ()
   local match = {}
   for i=1,#self.match do
      table.insert (match, self.match[i].sn..':'..self.match[i].type)
   end
   local str = ([[
curr: %d
match: %s
final: %s
graph:
]]):format (self.curr, table.concat (match, ', '),
            table.concat (self.final, ', '))
   for k,v in pairs (self.graph) do
      str = str..('  %s -> %s\n'):format (k, table.concat (v, ', '))
   end
   return str
end

function automata:feed (e)
   assert (type (e) == 'table')
   assert (type (e.sn) == 'number')
   assert (type (e.type) == 'string')
   local tab = self.graph[e.type]
   if tab == nil then
      return false              -- no state change
   end
   local next = tab[self.curr]
   if next == nil or next <= 0 or next == self.curr then
      return false              -- no state change
   end
   -- Change state.
   self.curr = next
   table.insert (self.match, e)
   return true
end

function automata:reset ()
   self.curr = 1
   self.match = {}
end

return automata

-- Local Variables:
-- eval: (add-hook 'write-file-functions 'time-stamp)
-- time-stamp-start: "_G.version = '"
-- time-stamp-format: "%:y-%02m-%02d, %02H:%02M"
-- time-stamp-time-zone: "UTC'"
-- time-stamp-end: " UTC"
-- End:
