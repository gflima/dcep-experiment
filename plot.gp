#!/usr/bin/env gnuplot
# Copyright (C) 2018 PUC-Rio/LAC
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

set terminal pdf enhanced font "arial" #size 5.27,6.69
set output "plot.pdf"
set size ratio 0.4

set style fill solid 0.3
set style histogram errorbars gap 2 title font ",10" offset character 0,1
set xtics font ",10"
set ytics font ",10"
set key top left font ",10"
set grid

set title "Running-time vs. number of failures"
set xlabel "Failures" offset 0,-2
set ylabel "Minutes"

plot newhistogram "Pattern 0" lt 1,\
     "pat0.data"   using 2:3:4:xtic(1) with hist title "up",\
     "pat0.data"   using 5:6:7 with hist title "down",\
     "pat0.x.data" using 2:3:4 with hist title "up-x",\
     "pat0.x.data" using 5:6:7 with hist title "down-x",\
     newhistogram "Pattern 1" lt 1,\
     "pat1.data"   using 2:3:4:xtic(1) with hist notitle,\
     "pat1.data"   using 5:6:7 with hist notitle,\
     "pat1.x.data" using 2:3:4 with hist notitle,\
     "pat1.x.data" using 5:6:7 with hist notitle,\
     newhistogram "Pattern 2" lt 1,\
     "pat2.data"   using 2:3:4:xtic(1) with hist notitle,\
     "pat2.data"   using 5:6:7 with hist notitle,\
     "pat2.x.data" using 2:3:4 with hist notitle,\
     "pat2.x.data" using 5:6:7 with hist notitle

set title "Maximum memory used vs. number of failures"
set xlabel "Failures" offset 0,-2
set ylabel "Megabytes"
set yrange [:20000]
set logscale y

plot newhistogram "Pattern 0" lt 1,\
     "pat0.data"   using 8:9:10:xtic(1) with hist title "up",\
     "pat0.data"   using 11:12:13 with hist title "down",\
     "pat0.x.data" using 8:9:10   with hist title "up-x",\
     "pat0.x.data" using 11:12:13 with hist title "down-x",\
     newhistogram "Pattern 1" lt 1,\
     "pat1.data"   using 8:9:10:xtic(1) with hist notitle,\
     "pat1.data"   using 11:12:13 with hist notitle,\
     "pat1.x.data" using 8:9:10   with hist notitle,\
     "pat1.x.data" using 11:12:13 with hist notitle,\
     newhistogram "Pattern 2" lt 1,\
     "pat2.data"   using 8:9:10:xtic(1) with hist notitle,\
     "pat2.data"   using 11:12:13 with hist notitle,\
     "pat2.x.data" using 8:9:10   with hist notitle,\
     "pat2.x.data" using 11:12:13 with hist notitle
